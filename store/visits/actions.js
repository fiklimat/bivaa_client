function validateProperty (data, propertyName) {
    const property = data[propertyName];
    if (!property) throw 'missing required property ' + propertyName
    return property;
}
function getValidatedVisit(data){
    return {
        id: validateProperty(data, 'id'),
        doctorId: validateProperty(data, 'doctorId'),
        specialityId: validateProperty(data, 'specialityId'),
        name: validateProperty(data, 'name'),
        length: validateProperty(data, 'length')
    }
}
export default {
    async loadVisits(context) {
        const response = await this.$axios.$get('/visit/all');
        const visits = [];
        for (const key in response)
            visits.push(getValidatedVisit(response[key]));
        context.commit('setVisits', visits);
    },
    async loadSelectedVisit(context, payload) {
        const response = await this.$axios.$get('visit/detail/' + payload);
        return getValidatedVisit(response);
    }
};
