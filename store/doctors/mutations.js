export default {
    setDoctors(state, payload) {
        state.doctors = payload;
    },
    setLoggedDoctor(state, payload) {
        state.loggedDoctor = payload;
    }
};
