export const state = () => ({
    doctors: [],
    loggedDoctor: {
        authenticationToken: "",
        doctor: {}
    }
})
