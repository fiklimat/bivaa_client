function validateProperty (data, propertyName) {
    const property = data[propertyName];
    if (!property) throw 'missing required property ' + propertyName
    return property;
}
function getValidatedDoctor (data) {
    return {
        id: validateProperty(data, 'id'),
        name: validateProperty(data, 'name'),
        surname: validateProperty(data, 'surname'),
        title: validateProperty(data, 'title'),
        specialityIds: validateProperty(data, 'specialityIds'),
    };
}
export default {
    async loadDoctors(context) {
        const response = await this.$axios.$get('/doctor/all');
        const doctors = []
        for (const key in response) {
            const doctor = getValidatedDoctor(response[key]);
            doctor.fullName = doctor.title + ' ' + doctor.name + ' ' + doctor.surname
            doctors.push(doctor);
        }
        context.commit('setDoctors', doctors);
    },
    async loadSelectedDoctor(context, payload) {
        const response = await this.$axios.$get('doctor/detail/' + payload);
        const doctor = getValidatedDoctor(response);
        doctor.fullName = doctor.title + ' ' + doctor.name + ' ' + doctor.surname
        return doctor;
    },
    async authenticate(context,payload) {
        const token = window.btoa(payload.login.username + ":" + payload.login.password);
        const response = await this.$axios.get('/doctor/detail/username/' + payload.login.username, {headers: {'Authorization': `Basic ${token}`}});
        const loggedDoctor = {
            authenticationToken: token,
            doctor: getValidatedDoctor(response.data),
        }
        loggedDoctor.doctor.fullName = response.data.title + ' ' + response.data.name + ' ' + response.data.surname;
        context.commit('setLoggedDoctor', loggedDoctor);
    }
};
