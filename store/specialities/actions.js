function validateProperty (data, propertyName) {
    const property = data[propertyName];
    if (!property) throw 'missing required property ' + propertyName
    return property;
}
function getValidatedSpeciality(data){
    return {
        id: validateProperty(data, 'id'),
        name: validateProperty(data, 'name'),
    }
}
export default {
    async loadSpecialities(context) {
        const response = await this.$axios.$get('/speciality/all');
        const specialities = [];
        for (const key in response)
            specialities.push(getValidatedSpeciality(response[key]));
        context.commit('setSpecialities', specialities);
    },
    async loadSelectedSpeciality(context, payload) {
        const response = await this.$axios.$get('/speciality/detail/' + payload);
        return getValidatedSpeciality(response);
    }
};
