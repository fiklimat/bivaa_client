function validateProperty (data, propertyName) {
    const property = data[propertyName];
    if (!property) throw 'missing required property ' + propertyName
    return property;
}
function getValidatedReservation(data){
    return {
        id: validateProperty(data, 'id'),
        typeofvisitId: validateProperty(data, 'typeofvisitId'),
        patientName: validateProperty(data, 'patientName'),
        patientSurname: validateProperty(data, 'patientSurname'),
        email: validateProperty(data, 'email'),
        phone: validateProperty(data, 'phone'),
        insurancecompanyId: validateProperty(data, 'insurancecompanyId'),
        note: data.note
    }
}
function getValidatedReservationWithInterval(data){
    return{
        id: validateProperty(data, 'id'),
        doctorId: validateProperty(data, 'doctorId'),
        specialityId: validateProperty(data, 'specialityId'),
        date: validateProperty(data, 'date').split('.').join('-'),
        startTime: validateProperty(data, 'startTime'),
        endTime: validateProperty(data, 'endTime'),
        reservationId: validateProperty(data, 'reservationId'),
        typeofvisitId: validateProperty(data, 'typeofvisitId'),
        patientName: validateProperty(data, 'patientName'),
        patientSurname: validateProperty(data, 'patientSurname'),
        email: validateProperty(data, 'email'),
        phone: validateProperty(data, 'phone'),
        insurancecompanyId: validateProperty(data, 'insurancecompanyId'),
        note: data.note
    }
}
export default {
    async sendReservation (context, payload) {
        const newRequest = {
            intervalId: validateProperty(payload, 'intervalId'),
            reservation: getValidatedReservation(payload.reservation)
        };
        const response = await this.$axios.post('/reservation/create', newRequest);
        const createdReservation = getValidatedReservation(response.data);
        context.commit("addReservation", createdReservation);
        return createdReservation;
    },
    async deleteReservation(context, payload) {
        try {
            const response = await this.$axios.delete('/reservation/delete/' + payload.reservationId);
            return response.status;
        }
        catch (error) {
            return error.response.status;
        }
    },
    async loadReservationsForDoctor (context, payload) {
        const response = (await this.$axios.get('/reservation/all/filtered/' + payload.doctorId, {headers: {'Authorization': `Basic ${context.rootState.doctors.loggedDoctor.authenticationToken}`}})).data;
        const reservations = [];
        for (const key in response)
            reservations.push(getValidatedReservationWithInterval(response[key]));
        context.commit("setReservations", reservations);
    }
};
