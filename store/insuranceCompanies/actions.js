function validateProperty (data, propertyName) {
    const property = data[propertyName];
    if (!property) throw 'missing required property ' + propertyName
    return property;
}
function getValidatedInsuranceCompany(data){
    return {
        id: validateProperty(data, 'id'),
        name: validateProperty(data, 'name'),
        shortname: data.shortname,
    }
}
export default {
    async loadInsuranceCompanies(context) {
        try {
            const response = await this.$axios.$get('/insuranceCompany/all');
            const insuranceCompanies = [];
            for (const key in response) {
                const insuranceCompany = getValidatedInsuranceCompany(response[key]);
                if (insuranceCompany.id === 1)
                    insuranceCompany.display = insuranceCompany.name;
                else
                    insuranceCompany.display = insuranceCompany.id + ' - ' + insuranceCompany.shortname + ' (' + insuranceCompany.name + ')'
                insuranceCompanies.push(insuranceCompany);
            }
            context.commit('setInsuranceCompanies', insuranceCompanies);
        }
        catch (error){
            throw(error);
        }
    }
};
