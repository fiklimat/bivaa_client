export default {
    setWorkingHours(state, payload) {
        state.workingHours = payload;
    },
    addWorkingHours(state, payload) {
        state.workingHours.push(payload);
    }
}
