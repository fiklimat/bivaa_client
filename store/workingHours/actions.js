function validateProperty (data, propertyName) {
    const property = data[propertyName];
    if (!property) throw 'missing required property ' + propertyName
    return property;
}
function getValidatedWorkingHours(data){
    return {
        id: validateProperty(data, 'id'),
        doctorId: validateProperty(data, 'doctorId'),
        specialityId: validateProperty(data, 'specialityId'),
        day: validateProperty(data, 'day'),
        startTime: validateProperty(data, 'startTime'),
        endTime: validateProperty(data, 'endTime')
    }
}
export default {
    async LoadWorkingHoursForDoctorSpeciality (context, payload) {
        const data = (await this.$axios.get('/workingHours/all/filtered/' + payload.doctorId + '/' + payload.specialityId, {headers: {'Authorization': `Basic ${context.rootState.doctors.loggedDoctor.authenticationToken}`}})).data;
        const workingHours = [];
        for (const key in data)
            workingHours.push(getValidatedWorkingHours(data[key]));
        context.commit("setWorkingHours", workingHours);
    },
    async SendWorkingHours (context, payload) {
        const newRequest = {
            doctorId: validateProperty(context.rootState.doctors.loggedDoctor.doctor, 'id'),
            specialityId: validateProperty(payload, 'specialityId'),
            day: validateProperty(payload, 'day'),
            startTime: validateProperty(payload.interval, 'startTime'),
            endTime: validateProperty(payload.interval, 'endTime')
        }
        const response = (await this.$axios.post('workingHours/create', newRequest, {headers: {'Authorization': `Basic ${context.rootState.doctors.loggedDoctor.authenticationToken}`}}));
        console.log('response');
        console.log(response);
        context.commit('addWorkingHours', getValidatedWorkingHours(response.data));
    },
    async DeleteWorkingHours (context, payload) {
        await this.$axios.delete('workingHours/delete/' + payload.id, {headers: {'Authorization': `Basic ${context.rootState.doctors.loggedDoctor.authenticationToken}`}});
    }
};
