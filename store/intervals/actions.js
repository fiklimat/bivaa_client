function validateProperty (data, propertyName) {
    const property = data[propertyName];
    if (!property) throw {message: 'missing required property ' + propertyName, response: {status: null}};
    return property;
}
function getValidatedInterval(data){
    return {
        id: validateProperty(data, 'id'),
        doctorId: validateProperty(data, 'doctorId'),
        specialityId: validateProperty(data, 'specialityId'),
        date: validateProperty(data, 'date').split('.').join('-'),
        startTime: validateProperty(data, 'startTime'),
        endTime: validateProperty(data, 'endTime'),
        reservationId: data.reservationId,
    }
}
export default {
    async loadFreeIntervalsForDoctorSpeciality(context, payload) {
        const response = await this.$axios.$get('/interval/all/free/'+ payload.doctorId + '/' + payload.specialityId);
        const freeIntervals = [];
        for (const key in response)
            freeIntervals.push(getValidatedInterval(response[key]));
        context.commit('setFreeIntervals', freeIntervals);
    },
        async getLoadedFreeIntervals(context) {
        const temp = context.state.freeIntervals;
        console.log("action get");
        console.log(temp);
        return temp;
    },
    async loadSelectedInterval(context, payload) {
        const response = await this.$axios.$get('interval/detail/free/' + payload);
        return getValidatedInterval(response);
    }
};
